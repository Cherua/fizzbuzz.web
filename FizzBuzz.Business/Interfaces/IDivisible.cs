﻿namespace FizzBuzz.Business.Interfaces
{
    public interface IDivisible
    {
        bool IsDivisible(int number);
        string GetMessage(int number);
    }
}

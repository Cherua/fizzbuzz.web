﻿using System;
namespace FizzBuzz.Business.Interfaces
{
    public interface ICheckForDay
    {
        bool IsWednesday(DayOfWeek daytoday);
    }
}

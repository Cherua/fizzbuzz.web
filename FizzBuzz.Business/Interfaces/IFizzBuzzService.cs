﻿using System.Collections.Generic;

namespace FizzBuzz.Business.Interfaces
{
    public interface IFizzBuzzService
    {
        IList<string> GetFizzBuzzList(int number);
    }
}

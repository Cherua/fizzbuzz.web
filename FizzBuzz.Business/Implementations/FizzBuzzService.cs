﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using FizzBuzz.Business.Interfaces;

namespace FizzBuzz.Business.Implementations
{
    public class FizzBuzzService : IFizzBuzzService
    {
        private IList<IDivisible> rules;
        public FizzBuzzService(IList<IDivisible> rules)
        {
            this.rules = rules;
        }

        public IList<string> GetFizzBuzzList(int number)
        {
            var fizzbuzzlist = new List<string>();

            for (int index = 1; index <= number; index++)
            {
                var message = "";
                var matchedrules = this.rules.Where(p => p.IsDivisible(index));
                if(matchedrules.Any())
                {
                    var values = matchedrules.Select(p => p.GetMessage(index));
                    message = string.Join(" ", values);
                }
                else
                {
                    message = index.ToString();
                }
                fizzbuzzlist.Add(message);
            }
            return fizzbuzzlist;
        }
    }
}

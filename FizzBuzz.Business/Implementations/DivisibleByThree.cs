﻿using System;
using FizzBuzz.Business.Interfaces;

namespace FizzBuzz.Business.Implementations
{
    public class DivisibleByThree : IDivisible
    {
        private readonly ICheckForDay dayToday;
        public DivisibleByThree(ICheckForDay dayToday)
        {
            this.dayToday = dayToday;
        }

        public bool IsDivisible(int number)
        {
            return number % 3 == 0;
        }

        public string GetMessage(int number)
        {
            return (this.dayToday.IsWednesday(DateTime.Today.DayOfWeek)) == true ? "wizz" : "fizz";
        }
    }
}

﻿using System;

using FizzBuzz.Business.Interfaces;
using FizzBuzz.Business.Implementations;

using NUnit.Framework;


namespace FizzBuzz.Business.UnitTests
{
    [TestFixture]
    public class CheckForDayUnitTest
    {
        private ICheckForDay dayToday;

        [SetUp]
        public void LoadContext()
        {
            dayToday = new CheckForDay();
        }

        [TestCase(DayOfWeek.Wednesday, ExpectedResult = true)]
        public bool IsWednesday_Should_Return_True_If_Today_Is_Wednesday(DayOfWeek day)
        {
            //Act
            var result = dayToday.IsWednesday(day);

            //Assert
            return result;
        }

        [TestCase(DayOfWeek.Monday, ExpectedResult = false)]
        [TestCase(DayOfWeek.Tuesday, ExpectedResult = false)]
        [TestCase(DayOfWeek.Thursday, ExpectedResult = false)]
        [TestCase(DayOfWeek.Friday, ExpectedResult = false)]
        [TestCase(DayOfWeek.Saturday, ExpectedResult = false)]
        [TestCase(DayOfWeek.Sunday, ExpectedResult = false)]
        public bool IsWednesday_Should_Return_False_If_Today_Is_Not_Wednesday(DayOfWeek day)
        {
            //Act
            var result = dayToday.IsWednesday(day);

            //Assert
            return result;
        }
    }
}

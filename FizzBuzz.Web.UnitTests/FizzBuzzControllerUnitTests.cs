﻿using System.Collections.Generic;

using FizzBuzz.Web.Controllers;
using FizzBuzz.Web.Models;
using FizzBuzz.Business.Interfaces;

using NUnit.Framework;
using Moq;

using PagedList;
using System.Web.Mvc;

namespace FizzBuzz.Web.UnitTest
{
    [TestFixture]
    public class FizzBuzzControllerUnitTests
    {
        private FizzBuzzController fizzBuzzController;
        private Mock<IFizzBuzzService> service;
        public FizzBuzzModel fizzBuzzModel;
        List<string> fizzbuzzlist = new List<string>(new string[] {"1", "2", "fizz", "4", "buzz","fizz", "7", "8", "fizz", "buzz",
            "11", "fizz", "13", "14", "fizz buzz" });

        [SetUp]
        public void LoadContext()
        {
            service = new Mock<IFizzBuzzService>();
            service.Setup(m => m.GetFizzBuzzList(It.IsAny<int>())).Returns(fizzbuzzlist);
            fizzBuzzController = new FizzBuzzController(service.Object);
            fizzBuzzModel = new FizzBuzzModel();
        }

        [TestCase]
        public void FizzBuzzList_Should_Return_Index_View_When_ModelState_Is_Invalid()
        {
            //Arrange
            fizzBuzzModel.UserInput = -1;
            int pagenumber = 1;
            fizzBuzzController.ModelState.AddModelError("Range", "Please enter a value within the range 1 to 1000");

            //Act
            var output = fizzBuzzController.FizzBuzzList(fizzBuzzModel, pagenumber) as ViewResult;

            //Assert
            Assert.AreEqual("Index", output.ViewName);
            Assert.IsFalse(fizzBuzzController.ModelState.IsValid);
            Assert.IsNull(fizzBuzzModel.ResultList);
            service.Verify(m => m.GetFizzBuzzList(It.IsAny<int>()), Times.Exactly(0));
        }

        [TestCase]
        public void FizzBuzzList_Should_Render_View_With_Paged_FizzBuzzList_When_Valid_Input_Is_Given()
        {
            //Arrange
            fizzBuzzModel.UserInput = 15;
            int pagenumber = 1;
            int pagesize = 20;
            var pagedlist = fizzbuzzlist.ToPagedList(pagenumber, pagesize);

            //Act
            var output = fizzBuzzController.FizzBuzzList(fizzBuzzModel, pagenumber) as ViewResult;

            //Assert
            Assert.AreEqual("Index", output.ViewName);
            Assert.IsNotNull(fizzBuzzModel.ResultList);

            Assert.AreEqual(pagedlist.GetType(), fizzBuzzModel.ResultList.GetType());
            Assert.AreEqual(pagedlist, fizzbuzzlist.ToPagedList(1, 20));

            service.Verify(m => m.GetFizzBuzzList(It.IsAny<int>()), Times.Exactly(1));
        }

        [TestCase]
        public void FizzBuzzList_Should_Render_View_With_Paged_FizzBuzzList_Based_On_Page_Number()
        {
            //Arrange
            fizzBuzzModel.UserInput = 15;
            int pagenumber = 2;
            int pagesize = 5;
            var pagedlist = fizzbuzzlist.ToPagedList(pagenumber, pagesize);

            //Act
            var output = fizzBuzzController.FizzBuzzList(fizzBuzzModel, pagenumber) as ViewResult;

            //Assert
            Assert.AreEqual("Index", output.ViewName);
            Assert.IsNotNull(fizzBuzzModel.ResultList);

            Assert.AreEqual(pagedlist.GetType(), fizzBuzzModel.ResultList.GetType());

            Assert.AreEqual(pagedlist, fizzbuzzlist.ToPagedList(2, 5));

            service.Verify(m => m.GetFizzBuzzList(It.IsAny<int>()), Times.Exactly(1));
        }
    }
}
